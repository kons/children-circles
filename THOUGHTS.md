Think out loud, scan of initial reqs
-----------------------------------------
Goal is to pull out entities, datastructures, approaches from the spec.

* n children stand in a circle
  - 'circle' a datastructure, linked-list, dequeue

* Starting with a given child and working clockwise, each child gets a 
 sequential number, which we will refer to as it’s id. 
  - array linked list.  clockwise (increment id)
  
* Then starting with the first child, they count out from 1 until k. The 
   k’th child is now out and leaves the circle. The count starts again 
   with the child immediately next to the eliminated one.
  - pointer to child, can't be an index
  - what happens when child is removed and pointer lands there?
  - is there a default data structure that will do this?
   
* Children are so removed from the circle one by one. The winner is the 
 child left standing last.
  - Can Winner be determined ahead of time?
  
  
Write a  method on a new class, which, when given n and k, returns the 
sequence of children as they go out, and the id of the winning child. 


Possible Entities
-------------------
Circle Builder
Child has an id

More thoughts on Datastructures
----------------------------------
Ring Datastructure (Queue) matters less than I thought (at least to start with).  
If clockwise == incremental id's, then a traditional arraylist (and its iterator)
will suffice.
And we are removing elements from middle of list, which is going to fit in an arraylist. (optimise later)
 
Might be worth discussing when pairing, if the requirements change for picking
kids in a different order or changing direction, then another data structure 
may be more appropriate.



Known Limitations
-------------------
- Positive int and null Validation on the n and k params when creating a circle
- Enhancement: rather than use the listiterator and iterate to the next child for as many iterations, replace with a 
                calculated index of where the next child should be. 
- No validation on input args on the main app