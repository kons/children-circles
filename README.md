This is a coding exercise.

See my [THOUGHTS](THOUGHTS.md) on the [PROBLEM](PROBLEM.md).

### Tech Stack
Written with Groovy 2.4 & Java 8.

Gradle used for the build tool, with Spock for BDD style tests.

All you need is Java 8 installed. Gradle is bootstrapped with its wrapper, and all other dependencies are downloaded

### Running tests
`./gradlew build`

### Running the app
Run `./gradlew installDist` to create the startup scripts and jars for the app.  

Parameters to the startup script are the number of children followed by the interval each turn

```bash
build/install/children-circles/bin/children-circles 12 7
```