package org.outrospective.irx.childrencircles

import groovy.transform.Immutable

@Immutable
class Child {
    int id
}
