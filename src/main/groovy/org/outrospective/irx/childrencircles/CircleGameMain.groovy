package org.outrospective.irx.childrencircles

class CircleGameMain {
    private Circle circle
    private final int numberOfChildren
    private final int selectionInterval

    static void main(String[] args) {
        def game = new CircleGameMain(args[0].toInteger(), args[1].toInteger())

        println "Sequence of people leaving"
        game.leavingSequence().each { println it }

        println "\nWinner is child with id ${game.winner()}"
    }

    CircleGameMain(int numberOfChildren, int selectionInterval) {
        this.selectionInterval = selectionInterval
        this.numberOfChildren = numberOfChildren
    }

    List<Child> leavingSequence() {
        circle = new Circle(numberOfChildren, selectionInterval)

        def result = []
        while (circle.size() != 0) {
            result << circle.pullNextChild()
        }
        return result
    }

    int winner() {
        leavingSequence().last().id
    }
}
