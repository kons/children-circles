package org.outrospective.irx.childrencircles

class Circle {
    private final int numberOfChildren
    private final LinkedList<Child> children
    private final int selectionInterval
    private ListIterator<Child> iterator

    Circle(int numberOfChildren, int selectionInterval) {
        this.selectionInterval = selectionInterval
        this.numberOfChildren = numberOfChildren
        children = makeChildren()
        iterator = children.listIterator()
    }

    private def makeChildren() {
        (1..numberOfChildren).collect { new Child(id: it) }
    }

    int size() {
        children.size()
    }

    Child pullNextChild() {
        Child child = findTheNextChild()

        iterator.remove()

        return child
    }

    private Child findTheNextChild() {
        def lastChild

        (1..selectionInterval).forEach {
            if (!iterator.hasNext()) {
                iterator = children.listIterator()
            }
            lastChild = iterator.next()
        }

        return lastChild
    }
}
