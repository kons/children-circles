package org.outrospective.irx.childrencircles

import org.outrospective.irx.childrencircles.util.TestUtil
import spock.lang.Specification

class CircleSpec extends Specification {

    def "Creates a circle of given size"() {
        given:
        def numberOfChildren = 6

        when:
        def circle = new Circle(numberOfChildren, TestUtil.positiveRandomInteger())

        then:
        circle.size() == numberOfChildren

        and: "each child has a unique id"
        def children = circle.children
        def ids = children*.id

        ids.unique() == ids

        and: "child ids start from 1"
        children.every { child -> child.id >= 1 }

        and: "child ids end at the number of children"
        children.every { child -> child.id <= numberOfChildren }
    }

    def "children are removed from the circle"() {
        setup:
        def selectionInterval = 3
        def turnsBeforeCirclingTheLoop = 4
        def numberOfChildren = selectionInterval * turnsBeforeCirclingTheLoop

        def circle = new Circle(numberOfChildren, selectionInterval)

        when: "starting with the first child, count out 1 until $selectionInterval"
        def child = circle.pullNextChild()

        then: "first removed child should have the same index as the gap size ($selectionInterval)"
        child.id == selectionInterval

        and: "size of children has decremented by one"
        circle.size() == numberOfChildren - 1

        and: "next removed child should have the same index as the twice the gap size"
        def nextChild = circle.pullNextChild()
        nextChild.id == 2 * selectionInterval

        and: "size of children has decremented by two"
        circle.size() == numberOfChildren - 2
    }

    def "Selection continues at the beginning of the circle"() {
        setup:
        def selectionInterval = 5
        def turnsBeforeCirclingTheLoop = 2
        def additionalChildrenOffset = 1
        def numberOfChildren = selectionInterval * turnsBeforeCirclingTheLoop + additionalChildrenOffset  // 11

        def circle = new Circle(numberOfChildren, selectionInterval)

        when: "we have enough turns to circle the loop"
        def lastChild
        def turns = turnsBeforeCirclingTheLoop + 1
        turns.times { lastChild = circle.pullNextChild() }

        then: "last child selected will be at the beginning of the circle with an id that is offset by the number " +
                "of extra children at the end of the circle who dont neatly fit into the selection interval"
        lastChild.id == selectionInterval - additionalChildrenOffset  // 4
    }

    def "pulling all the children will empty the circle"() {
        setup:
        def numberOfChildren = 7
        def circle = new Circle(numberOfChildren, 1)

        when:
        numberOfChildren.times { circle.pullNextChild() }

        then:
        circle.size() == 0
    }


    def "the interval between children is larger than the size of the circle"() {
        setup:
        def circle = new Circle(5, 7)

        when:
        5.times { circle.pullNextChild() }

        then:
        circle.size() == 0
    }

    def "handle case when count of children is double the selection interval"() {
        setup:
        def circle = new Circle(10, 5)

        when:
        def firstThreeKids = (1..3).collect { circle.pullNextChild().id }

        then:
        firstThreeKids == [ 5, 10, 6 ]
    }
}
