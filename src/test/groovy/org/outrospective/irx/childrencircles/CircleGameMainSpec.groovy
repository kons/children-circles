package org.outrospective.irx.childrencircles

import spock.lang.Specification

class CircleGameMainSpec extends Specification {

    def "retrieves sequence of leaving children"() {
        setup:
        def game = new CircleGameMain(12, 7)

        when:
        def sequence = game.leavingSequence()

        then:
        sequence*.id == [7, 2, 10, 6, 4, 3, 5, 9, 1, 8, 11, 12]
    }

    def "retrieves the winning child id"() {
        setup:
        def game = new CircleGameMain(12, 7)

        when:
        def winner = game.winner()

        then:
        winner == 12
    }
}
